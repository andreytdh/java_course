package Lesson15;

import Lesson2.Cat;

import java.util.*;

public class ArrayVSLinkedList {

    public static void add(List list) {
        for (int i = 0; i < 1000000; i++) {
            list.add(0, 1);
        }
    }

    public static long getTime(List list) {
        Date startTime = new Date();
        add(list);
        Date finishTime = new Date();
        long delay = finishTime.getTime() - startTime.getTime();
        return delay;
    }

    public static void main(String[] args) {
        System.out.println(getTime(new ArrayList<Integer>()));
        System.out.println(getTime(new LinkedList<Integer>()));


        ArrayList<Object> list = new ArrayList<>();
        list.add("asdds");
        list.add(1);


        ArrayList<Cat> cats = new ArrayList<>();
        Cat murzik = new Cat();
        cats.add(murzik);
        //cats.add("asd");
        list.add(murzik);


    }
}

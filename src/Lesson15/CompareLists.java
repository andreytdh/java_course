package Lesson15;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class CompareLists {

    public static void insert100000(List list) {
        for (int i = 0; i < 100000; i++) {
            list.add("test");
        }
    }

    public static void remove100000(List list) {
        for (int i = 0; i < 100000; i++) {
            list.remove(0);
        }
    }

    public static long getTime(List list) {
        Date startTime = new Date();
        remove100000(list);
        Date finishTime = new Date();
        long delay = finishTime.getTime() - startTime.getTime();
        return delay;
    }

    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<>();
        insert100000(list1);
        LinkedList<String> list2 = new LinkedList<>();
        insert100000(list2);

        System.out.println(getTime(list1));
        System.out.println(getTime(list2));
    }
}

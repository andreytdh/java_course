package Lesson15;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class MapVsSet {

    public static void main(String[] args) {

        HashSet<String> example = new HashSet<>();
        //System.out.println(example.isEmpty());
        example.add("test");
        //System.out.println(example.contains("test"));
        example.add("test");
        example.add("test123");
        example.add("Ivanov Alexander 4002 123456");
        example.add("Petrov Oleg 4002 235120");
        System.out.println(example);

        for (String entry: example) {
            System.out.println(entry);
        }


        System.out.println(" ");

        HashMap<Integer, String> passports = new HashMap<>();
        passports.put(44012313, "Ivanov Alexander Ivanovich");
        passports.put(1231231, "Petrov Oleg");
        System.out.println(passports.get(1231231));
        passports.replace(1231231, "Petrova Olga");
        System.out.println(passports);

        System.out.println(passports.entrySet());
        System.out.println(passports.keySet());
        System.out.println(passports.values());

        for (Map.Entry<Integer, String> entry: passports.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        }
    }



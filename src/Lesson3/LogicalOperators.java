package Lesson3;



public class LogicalOperators {

    public static void main(String[] args) {
        boolean a = false;
        boolean b = true;

        // logical AND &
        System.out.println(a&b);
        // logical OR |
        System.out.println(a|b);
        // equals
        System.out.println(a==b);
        // not equals
        System.out.println(a!=b);

        // short logical AND &&
        System.out.println(a&&b);
        // short logical OR ||
        System.out.println(a||b);
        //
        System.out.println("answer:");

        System.out.println(true&&(false|true||true)&false);
    }

}

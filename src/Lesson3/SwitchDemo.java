package Lesson3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SwitchDemo {

    public static void main(String[] args) throws IOException {
    int month = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        month = Integer.parseInt(br.readLine());


    switch (month) {
        case 0:
            System.out.println("January");
            break;
        case 1:
            System.out.println("February");
            break;
        case 2:
            System.out.println("March");
            break;
        default:
            System.out.println("not correct");
    }
    }



}

package Lesson3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test2 {

    public static void main(String[] args) throws IOException {

        // if - then
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(br.readLine());
        int b = 3;
        int c = 1;

        if (a==3) {
            c = a + b;
            c = c + 3;
        }
        else if (a==0) {
            c = 5;
        }
        else if (a ==2 )  {
            a = 2;
        }
        else if (a == 1) {
            a = 3;
        }

        System.out.println(c);
    }

}

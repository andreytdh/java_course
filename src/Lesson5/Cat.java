package Lesson5;



public class Cat {
    static int count = 0;

    String name;
    int age;
    int weight;

    public Cat() {}

    public Cat (String name) {
        this.name = name;
    }

    public Cat(String name, int age, int weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public Cat(int age, int weight, String name) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public void printCat() {
        System.out.println("имя кота: " +name + " " + "возраст кота: " + age + " "+ "вес кота: " + weight);
    }


    public static void main(String[] args) {


        System.out.println(count);

        Cat cat5 = new Cat();
        cat5.name = "Murzik";
        cat5.age = 1;
        cat5.weight = 5;

        //System.out.println(cat5.name);
        //System.out.println(cat5.age);
        //System.out.println(cat5.weight);
        cat5.printCat();

        Cat cat2 = new Cat("Barsik", 2 , 7);
        Cat cat3 = new Cat(2, 8, "Rizik");
        cat2.printCat();
        cat3.printCat();

        Cat cat6 = new Cat("Basrik2");

    }

}

package Lesson16;

public class StackExample2 {
    public static void main(String[] args) {
        method1();
    }
    public static void method1(){
        method2();
    }

    public static void method2(){
        method3();
    }

    public static void method3(){
    StackTraceElement[] stack = Thread.currentThread().getStackTrace();

        for (StackTraceElement element: stack) {
            System.out.println(element.getMethodName());
        }
    }

}

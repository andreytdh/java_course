package Lesson16;

import java.util.Stack;

public class StackExample {
    //List

    public static void main(String[] args) {
        Stack<Integer> example = new Stack<>();
        System.out.println(example.empty());
        example.push(1);
        example.push(2);
        example.push(3);
        example.push(4);

        System.out.println(example.empty());
       // System.out.println(example.size());

       // System.out.println(example.get(0));
       // example.remove(0);
       // System.out.println(example);

        System.out.println("текущий стек: " + example);
        System.out.println(example.peek());
       // System.out.println(example.search(1));
        System.out.println("удаляем из стека: " + example.pop() + " >>> " + "текущий стек: " + example);
        System.out.println("удаляем из стека: " + example.pop() + " >>> " + "текущий стек: " + example);
        System.out.println("удаляем из стека: " + example.pop() + " >>> " + "текущий стек: " + example);
        System.out.println("удаляем из стека: " + example.pop() + " >>> " + "текущий стек: " + example);
        System.out.println(example.empty());

        //List: ArrayList, LinkedList, Stack (Vector)

    }



}

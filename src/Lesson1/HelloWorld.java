package Lesson1;

public class HelloWorld {
    static String name;

    public static String printHello(String name) {
        return ("Hello " + name);
    }

    public static void main(String[] args) {
        System.out.println(printHello("Timur"));
    }
}

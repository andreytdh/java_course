package Lesson13;

import java.util.HashMap;

public class Cats {

    String name;
    int age;

    public Cats(String name, int age) {
        this.age = age;
        this.name= name;
    }
    public static void main(String[] args) {
        Cats barsik = new Cats("Barsik", 2);
        Cats murzik = new Cats("Murzik", 3);

        HashMap<Integer, Cats> cats = new HashMap<>();
        cats.put(1, barsik);
        cats.put(2, murzik);

        System.out.println(cats.get(1).name);

    }

}

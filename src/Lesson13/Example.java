package Lesson13;

public class Example {
    private String name;
    private int x;

    private static int count;

    //конструктор по умолчанию
    public Example() {}

    public Example(String name) {
        this.name = name;
        Example.count = 1;
    }

    public String printName() {
        return this.name;
    }

    public void printNameonTheScreen() {
        System.out.println(this.name);
    }

    public static int printCount() {
        return Example.count;
    }

    public void initialize(String name) {
        this.name= name;
    }

    public static void main(String[] args) {

        printCount();


        Example link0 = new Example();
        link0.name = "test";

        link0.initialize("test");

        System.out.println(link0.printName());
        link0.printNameonTheScreen();

        //System.out.println(Example.count);


        Example link = new Example("test");
        System.out.println(link.x);


        //System.out.println(link.name);
        //System.out.println(Example.count);
    }

}

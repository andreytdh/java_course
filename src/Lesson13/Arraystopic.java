package Lesson13;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Arraystopic {

    //index > value
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(1);
        System.out.println(list.get(0));
        System.out.println(list.get(1));

        ArrayList<String> list2 = new ArrayList<>();
        list2.add("SPb\",6000000");


        // unique values
        HashSet<Integer> set = new HashSet<Integer>();
        set.add(1);
        set.add(1);
        set.add(2);


        System.out.println(set.size());


        //key > value
        HashMap<String, Integer> map = new HashMap<>();
        map.put("SPb", 6000000);
        map.put("Moscow", 11000000);


        System.out.println("Город Санкт-Петербург имеет " + map.get("SPb") + " жителей.");
        System.out.println(map.get("Tokio"));


    }
}
package Lesson7.test1;

public class Cat {
    String name;

    public Cat() { }

    public Cat(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Cat cat1 = new Cat("Murzik");
        System.out.println(cat1.name);

        Kitten kitten2 = new Kitten("asdasdasd", 1);
        System.out.println(kitten2.getSpeed());
        kitten2.setSpeed(-15);
        System.out.println(kitten2.getSpeed());
    }
}

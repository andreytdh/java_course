package Lesson7.test1;

public class Kitten extends Cat {
    protected int age;
    private int speed;

    public Kitten(String name, int age) {
        super(name);
        this.age = age;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        if (speed > 0) {
            this.speed = speed;
        }
        else System.out.println("отрицательная скорость не допустима");
    }


}

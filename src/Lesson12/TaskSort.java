package Lesson12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskSort {

    static int[] digits = new int[10];

    //в порядке возрастания
    public static void up(int[] array) {
        //  write your code here

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    //в порядке убывания
    public static void down(int[] array) {
        //  write your code here

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 10; i++) {
            digits[i] = Integer.parseInt(br.readLine());
        }
        up(digits);
        System.out.println("___________");
        down(digits);
    }
}

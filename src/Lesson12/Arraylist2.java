package Lesson12;

import java.util.ArrayList;

public class Arraylist2 {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<String>();

        ArrayList arrayList = new ArrayList();

        arrayList.add(3);
        arrayList.add("String");
        arrayList.add(list);

        ArrayList<Integer> digits = new ArrayList<>();
        ArrayList<Double> doubles = new ArrayList<>();
        //char > Character
        //boolean > Boolean
        //byte > Byte

        digits.add(3);
        Integer b = 1;
        digits.add(b);

    }
}

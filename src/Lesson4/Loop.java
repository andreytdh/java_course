package Lesson4;

public class Loop {


    public static void main(String[] args) {
        //int i = 1;

        for (int i = 0; i < 1; i++) {
            System.out.println("hello world");
        }
        //i++;
        // i=0:   0<10   i =1   sout
        // i =1; 1< 10  i = 2  sout
        //...
        // i = 9 9<10 ; i= 10 sout
        // i = 10 , 10 < 10


        ;
        System.out.println("______________");

        int a = 0;

        while (a < 0) {
            System.out.println("hello world");
            a++;
        }


        int count = 0;

        do {
            System.out.println("I've done it");
            count = 5;
        } while (count < 5);

    }
}


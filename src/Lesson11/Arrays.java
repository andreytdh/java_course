package Lesson11;

import java.util.ArrayList;

public class Arrays {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        //System.out.println(list.size());
        list.add("2"); //index0
        //System.out.println(list.size());
        list.add("3"); //index1
        list.set(1, "4"); // "3" > "4"
        System.out.println(list.get(0) + " " + list.get(1));

        System.out.println("_____________________");
        list.add(1, "1");
        System.out.println(list.size());

        System.out.println("_______________________");
        System.out.println(list.get(0) + " " + list.get(1) + " " + list.get(2));
        System.out.println(list.size());
        list.remove(1);
        System.out.println(list.get(0) + " " + list.get(1));

        for (int i = 0; i < list.size() ; i++) {
            list.add(0, "test");
        }

        int[] array = new int[10];
        array[1] = 2;
        array[5] = 0;

        System.out.println(array.length);

    }
}

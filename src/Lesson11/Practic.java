package Lesson11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Practic {

    static ArrayList<Integer> digits = new ArrayList<Integer>();
    //ArrayList<Double> digit = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 10; i++) {
            digits.add(Integer.parseInt(br.readLine()));
            //digits.add(Double.parseDouble(br.readLine()));
        }

        for (int i = digits.size() - 1; i >= 0; i--) {
            System.out.print(digits.get(i) + " ");
        }
    }
}

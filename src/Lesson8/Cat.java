package Lesson8;

public class Cat {
    String name;
    boolean isHungry;
    int hungryCount;


    public Cat(String name) {
        this.name = name;
        isHungry = true;
        hungryCount = 200;
    }


    public boolean isHungry() {
        if (this.hungryCount <= 0) {
            this.isHungry = false;
        }
        else this.isHungry = true;

        return isHungry;
    }

    public void eat(Fish fish) {
        //this.hungryCount = this.hungryCount - fish.getWeight();
        /* x = 5;
        x = x - 2;
        x -= 2;
        х = х +2;
        х + = 2;
        x *= 3;
         */


        this.hungryCount -= fish.getWeight();
    }


    public static void main(String[] args) {

        Cat barsik = new Cat("Barsik");

        System.out.println(barsik.isHungry);

        Fish fish1 = new Fish(50);
        Fish fish2 = new Fish(100);
        Fish fish3 = new Fish(150);

        barsik.eat(fish1);
        System.out.println(barsik.hungryCount);
        System.out.println(barsik.isHungry());
        barsik.eat(fish2);
        System.out.println(barsik.hungryCount);
        System.out.println(barsik.isHungry());
        barsik.eat(fish1);
        System.out.println(barsik.hungryCount);
        System.out.println(barsik.isHungry());
    }
}

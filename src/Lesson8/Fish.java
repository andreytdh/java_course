package Lesson8;

public class Fish {
    int weight;

    public Fish(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}

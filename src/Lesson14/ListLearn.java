package Lesson14;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListLearn {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();
        System.out.println(list.size());
        list.add(1);
        list.add(5);
        list.add(7);
        System.out.println(list.size());
        // 1 5 7  1 1 125  26 1 123 1 67 ...
        list.remove(1);
        list.add(1, 12);
        //  1 7  i=1: 7 >  i=2:
        //  0  1  2 3  4 5

        // int[] x = new int[10];
        //new int[16]
        //new array size = old array size* 1.5 +1;

        //data;
        //link previous;
        //link next;

        //     System.out.println(list.get(0));
        //     System.out.println(list.get(1));
        System.out.println(list);
        System.out.println(list.size());
        //System.out.println(list.get(3));

        for (Integer entry:list) {
            System.out.print(entry);
        }

        LinkedList<Integer> linkedList = new LinkedList<>();
        //System.out.println(linkedList.size());
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        //System.out.println(linkedList.getFirst());
        //System.out.println(linkedList.getLast());

        System.out.println(linkedList);
        for (Integer entry: linkedList) {
            System.out.print(entry);
        }
       // 1(previous null, next 2)  2 (previous 1, next 3)  3 (prev2 , next 4)  ...  10000 (prev 9999, next null)
       // 1(previous null, next 3)    3 (prev 1 , next 4) ...  10000 (prev 9999, next null)


    }
}
package Lesson2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Calc {

        public static void main(String[] args) throws IOException {
            int x;
            int y;

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please enter digit x");
            x = Integer.parseInt(br.readLine());
            System.out.println("Please enter digit y");
            y = Integer.parseInt(br.readLine());

            System.out.println("Sum: = " + (x+y));

            System.out.println("Program has been finished successfully");
        }
}
